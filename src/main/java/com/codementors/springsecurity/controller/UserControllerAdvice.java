package com.codementors.springsecurity.controller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.codementors.springsecurity.model.ApiError;

@RestControllerAdvice(assignableTypes = UserController.class)
public class UserControllerAdvice {

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  public ApiError handleConstraintViolation(final DataIntegrityViolationException exception) {
    return new ApiError(exception.getCause().getLocalizedMessage(), exception.getMessage());
  }
}
