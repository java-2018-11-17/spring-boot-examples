package com.codementors.springsecurity.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codementors.springsecurity.model.LocalUser;
import com.codementors.springsecurity.model.LocalUserId;
import com.codementors.springsecurity.model.LocalUserViews;
import com.codementors.springsecurity.repository.UserRepository;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class UserController {

  private final UserRepository userRepository;

  public UserController(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @RolesAllowed("ROLE_ADMIN")
  @GetMapping("/user/list")
  public List<LocalUser> getUsers() {
    return userRepository.findAll();
  }

  // wykorzystanie JsonView to zwrócenia określonych pól obiektu
  @PostMapping("/user")
  @JsonView(LocalUserViews.Id.class)
  public LocalUser addUser(@RequestBody final LocalUser localUser) {
    return userRepository.save(localUser);
  }

  // wykorzystanie osobnej reprezentacji do ogranieczenia zwracanych pól
  @PostMapping(value = "/user/add", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public LocalUserId addNewUser(@RequestBody final LocalUser user) {
    return new LocalUserId(userRepository.save(user).getId());
  }

}
