package com.codementors.springsecurity.model;

public class ApiError {

  private final String message;
  private final String details;

  public ApiError(final String message, final String details) {
    this.message = message;
    this.details = details;
  }

  public String getMessage() {
    return message;
  }

  public String getDetails() {
    return details;
  }
}
