package com.codementors.springsecurity.model;

public class LocalUserId {
  private String id;

  public LocalUserId(final String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(final String id) {
    this.id = id;
  }

}
