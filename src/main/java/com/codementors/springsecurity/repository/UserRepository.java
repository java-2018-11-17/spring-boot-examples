package com.codementors.springsecurity.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.codementors.springsecurity.model.LocalUser;

@Repository
public interface UserRepository extends CrudRepository<LocalUser, String> {

  List<LocalUser> findAll();
  Optional<LocalUser> findByUsername(String username);

}
