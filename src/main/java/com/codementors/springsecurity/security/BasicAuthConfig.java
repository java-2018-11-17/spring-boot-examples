package com.codementors.springsecurity.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.codementors.springsecurity.repository.UserRepository;

@EnableWebSecurity
public class BasicAuthConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    super.configure(http);

    http.httpBasic()
        .and()
        .csrf().disable();
  }

  @Bean
  public UserDetailsService userDetailsService(final UserRepository userRepository) {
    return new LocalUserDetailsService(userRepository);
  }

  @Autowired
  public void configureGlobal(final AuthenticationManagerBuilder auth, final LocalUserDetailsService userDetailsService) {
    final DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
    daoAuthenticationProvider.setUserDetailsService(userDetailsService);
    auth.authenticationProvider(daoAuthenticationProvider);
  }

}