package com.codementors.springsecurity.security;

import static java.util.Collections.singletonList;

import java.util.Optional;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.codementors.springsecurity.model.LocalUser;
import com.codementors.springsecurity.repository.UserRepository;

@Service
public class LocalUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;

  public LocalUserDetailsService(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    final Optional<LocalUser> localUser = userRepository.findByUsername(username);
    return localUser
        .map(this::mapToUser).orElseThrow(() -> new UsernameNotFoundException("No user with username " + username + " was found."));
  }

  private User mapToUser(final LocalUser user) {
    return new User(user.getUsername(), user.getPassword(), singletonList(new SimpleGrantedAuthority(user.getRole())));
  }
}
